import Rumahtangga from "../models/RumahtanggaModel.js";

export const bioRT = async(req, res) => {
    const { userId,nama,
        jenis_kelamin, 
        tempat_lahir, 
        tanggal_lahir, 
        tanggal_bergabung, 
        alamat,
        no_wa, no_rek, nama_bank, ktp, komitmen } = req.body;

    try {
        await Rumahtangga.update({
            nama: nama,
            jenis_kelamin: jenis_kelamin,
            tempat_lahir: tempat_lahir,
            tanggal_lahir:tanggal_lahir,
            tanggal_bergabung:tanggal_bergabung,
            alamat:alamat,
            no_wa:no_wa,
            no_rek:no_rek,
            nama_bank:nama_bank,
            ktp:ktp,
            komitmen: komitmen,
        },{
            where: {
                id: userId
            }
        });
        return res.status(200).json({msg: "Update Biodata Berhasil"});
    } catch (error) {
        return res.status(400).json({msg: "Update Biodata Gagal"});
    }
}