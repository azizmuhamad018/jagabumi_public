import Users from "../models/UsersModel.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import Komunitas from "../models/KomunitasModel.js";
import Rumahtangga from "../models/RumahtanggaModel.js";

export const getUsers = async(req, res) => {
    try {
        const users = await Users.findAll({
            attributes:['id','userid','levelid', 'nama', 'no_wa']
        });
        return res.status(200).json(users);
    } catch (error) {
        console.log(error);
    } 
}

export const RegisterRT = async(req, res) => {
    const { nama,
            jenis_kelamin, 
            tempat_lahir, 
            tanggal_lahir, 
            tanggal_bergabung, 
            alamat,
            no_wa, no_rek, nama_bank, ktp, komitmen,
            password, confPassword } = req.body;
    if(password !== confPassword) return res.status(400).json({msg: "Password dan Confirm Password tidak cocok"});
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);
    try {
        const saveRT = await Rumahtangga.create({
            idkom: 0,
            nama: nama,
            jenis_kelamin: jenis_kelamin,
            tempat_lahir: tempat_lahir,
            tanggal_lahir:tanggal_lahir,
            tanggal_bergabung:tanggal_bergabung,
            alamat:alamat,
            no_wa:no_wa,
            no_rek:no_rek,
            nama_bank:nama_bank,
            ktp:ktp,
            komitmen: komitmen,
        });
        await Users.create(
            {
                userid: saveRT.id,
                levelid: 5,
                nama: nama,
                no_wa: no_wa,
                password: hashPassword,
                status_approve: 0,
            }
        );
        return res.status(200).json({msg: "Register Berhasil"});
    } catch (error) {
        return res.status(400).json({msg: "Register Gagal"});
        console.log(error);
    }
}

export const RegisterKomunitas = async(req, res) => {
    const { nama_komunitas,  
            nama_ketua, 
            jenis_kelamin, 
            tempat_lahir, 
            tanggal_lahir, 
            tanggal_bergabung, 
            tanggal_rutin_pickup,
            alamat,
            no_wa, no_rek, nama_bank, ktp, komitmen,
            password, confPassword } = req.body;
    if(password !== confPassword) return res.status(400).json({msg: "Password dan Confirm Password tidak cocok"});
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);
    try {
        const saveKomunitas = await Komunitas.create({
            nama_komunitas: nama_komunitas,
            nama_ketua: nama_ketua,
            jenis_kelamin: jenis_kelamin,
            tempat_lahir: tempat_lahir,
            tanggal_lahir:tanggal_lahir,
            tanggal_bergabung:tanggal_bergabung,
            tanggal_rutin_pickup:tanggal_rutin_pickup,
            alamat:alamat,
            no_wa:no_wa,
            no_rek:no_rek,
            nama_bank:nama_bank,
            ktp:ktp,
            komitmen: komitmen,
        });
        await Users.create(
            {
                userid: saveKomunitas.id,
                levelid: 4,
                nama: nama_ketua,
                no_wa: no_wa,
                password: hashPassword,
                status_approve: 0,
            }
        );
        return res.status(200).json({msg: "Register Berhasil"});
    } catch (error) {
        return res.status(400).json({msg: "Register Gagal"});
        console.log(error);
    }
}

export const ChangePass = async(req, res) => {
    const { id, oldPassword, newPassword} = req.body;

    const user = await Users.findAll({
        where:{
            id: id
        }
    });
    const match = await bcrypt.compare(oldPassword, user[0].password);
    if(!match) return res.status(400).json({msg: "Password Lama Salah"});
    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(newPassword, salt);
    Users.update({ password: hashPassword }, { where: { id: id } });
    return res.status(200).json({msg: "Ganti Password Berhasil."});
}
export const Login = async(req, res) => {
    try {
        const user = await Users.findAll({
            where:{
                no_wa: req.body.no_wa
            }
        });
        const match = await bcrypt.compare(req.body.password, user[0].password);
        if(!match) return res.status(400).json({msg: "Wrong Password"});
        const Id = user[0].id;
        const userId = user[0].userid;
        const levelId = user[0].levelid;
        const nama = user[0].nama;
        const no_wa = user[0].no_wa;
        const status_approve = user[0].status_approve;
        if(status_approve == 0) return res.status(400).json({msg: "Akun belum di Approve."});
        const accessToken = jwt.sign({Id, userId, levelId, nama, no_wa}, process.env.ACCESS_TOKEN_SECRET,{
            expiresIn: '20s'
        });
        const refreshToken = jwt.sign({Id, userId, levelId, nama, no_wa}, process.env.REFRESH_TOKEN_SECRET,{
            expiresIn: '1d'
        });
        await Users.update({refresh_token: refreshToken},{
            where:{
                id: Id
            }
        });
        res.cookie('refreshToken', refreshToken,{
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        return res.status(200).json({refreshToken});
    } catch (error) {
        return res.status(404).json({msg:"Nomor Whatsapp tidak ditemukan"});
    }
}
 
export const Logout = async(req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if(!refreshToken) return res.sendStatus(204);
    const user = await Users.findAll({
        where:{
            refresh_token: refreshToken
        }
    });
    if(!user[0]) return res.sendStatus(204);
    const userId = user[0].id;
    await Users.update({refresh_token: null},{
        where:{
            id: userId
        }
    });
    res.clearCookie('refreshToken');
    return res.sendStatus(200);
}