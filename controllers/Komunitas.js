import Komunitas from "../models/KomunitasModel.js";
import request_to_kom from "../models/RequestKomunitas.js";
import Rumahtangga from "../models/RumahtanggaModel.js";
import db from "../config/Database.js";

export const bioKomunitas = async(req, res) => {
    const {
        userId,
        nama_komunitas,  
        nama_ketua, 
        jenis_kelamin, 
        tempat_lahir, 
        tanggal_lahir, 
        tanggal_bergabung, 
        tanggal_rutin_pickup,
        alamat,
        no_wa, no_rek, nama_bank, ktp, komitmen
    } = req.body
    try {
        await Komunitas.update({
            nama_komunitas: nama_komunitas,
            nama_ketua: nama_ketua,
            jenis_kelamin: jenis_kelamin,
            tempat_lahir: tempat_lahir,
            tanggal_lahir:tanggal_lahir,
            tanggal_bergabung:tanggal_bergabung,
            tanggal_rutin_pickup:tanggal_rutin_pickup,
            alamat:alamat,
            no_wa:no_wa,
            no_rek:no_rek,
            nama_bank:nama_bank,
            ktp:ktp,
            komitmen: komitmen
        }, {
            where: {
                id: userId
            }
        });
        return res.status(200).json({msg: "Update Biodata Berhasil"});
    } catch (error) {
        return res.status(400).json({msg: "Update Biodata Gagal"});
    }
}

export const getKomunitas = async(req, res) => {
    try {
        const data = await Komunitas.findAll();
        return res.status(200).json(data);
    } catch (error) {
        console.log(error);
    } 
}
export const getRequestKomunitas = async(req, res) => {
    try {
        await db.query("select * from request_to_komunitas as a left join users_rumahtangga as b on a.idrumahtangga = b.id where a.idkomunitas = "+ req.query.idkomunitas)
        .then(resData => {
            return res.status(200).json(resData[0]);
        });
    } catch (error) {
        console.log(error);
    } 
}
export const updateRequestKomunitas = async(req, res) => {
    const { id_req, idkomunitas, idrumahtangga, status } = req.query

    if (status == 1) {
        try {
            await request_to_kom.update({ status: 1}, {
                where: {
                    id: id_req
                }
            });
            await Rumahtangga.update({ idkom: idkomunitas }, {
                where: {
                    id: idrumahtangga
                }
            });
            return res.status(200).json("Update Request Berhasil.");
        } catch (error) {
            console.log(error);
        }
    } else {
        try {
            await request_to_kom.delete({
                where: {
                    id: id_req
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
}