import rekap_sampah from "../models/RekapSampahModel.js";
import { Op } from "sequelize";
import db from "../config/Database.js";

export const ListForCommunity = async(req, res) => {
    const {
        idkomunitas,
        start_date,
        end_date
    } = req.query;
    try {
        await db.query("select * from users_rekap_sampah as a left join users_rumahtangga as b on a.idrumahtangga = b.id where a.idkomunitas = "+ idkomunitas +" and a.createdAt between '"+ start_date +"' and '"+ end_date +"'").then(results => {
            return res.status(200).json(results);
        });
    } catch (error) {
        return res.status(400).json({msg: "gagal ambil data"});
    }  
}

export const ListByRT = async(req, res) => {
    const {
        idrumahtangga,
        start_date,
        end_date
    } = req.query;
    try {
        await db.query("select * from users_rekap_sampah where idrumahtangga = "+ idrumahtangga +" and createdAt between '"+ start_date +"' and '"+ end_date +"'")
        .then(resData => {
            return res.status(200).json(resData);
        });
    } catch (error) {
        return res.status(400).json({msg: "Input Data Gagal"});
    }  
}

export const inputSampah = async(req, res) =>{
    const { 
            idrumahtangga,
            idkomunitas, 
            jenis_sampah, 
            harga_per_kilo,
            jumlah, sub_total, estimasi_total, permintaan_pengambilan, jenis_pembayaran 
        } = req.body;
    console.log(req.file, req.body)
    try {
        await rekap_sampah.create({
            idrumahtangga: idrumahtangga,
            idkomunitas: idkomunitas,
            foto: req.file.filename,
            jenis_sampah: jenis_sampah,
            harga_per_kilo:harga_per_kilo,
            jumlah:jumlah,
            sub_total:sub_total,
            estimasi_total:estimasi_total,
            permintaan_pengambilan:permintaan_pengambilan,
            jenis_pembayaran:jenis_pembayaran
        });
        return res.status(200).json({msg: "Input Data Berhasil"});
    } catch (error) {
        return res.status(400).json({msg: "Input Data Gagal"});
    }
}