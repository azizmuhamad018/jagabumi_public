import { Sequelize } from "sequelize";
import db from "../config/Database.js";
 
const { DataTypes } = Sequelize;
 
const Users = db.define('users',{
    userid:{
        type: DataTypes.INTEGER
    },
    levelid:{
        type: DataTypes.INTEGER
    },
    nama:{
        type: DataTypes.STRING
    },
    no_wa:{
        type: DataTypes.STRING
    },
    password:{
        type: DataTypes.STRING
    },
    status_approve:{
        type: DataTypes.INTEGER
    },
    refresh_token:{
        type: DataTypes.TEXT
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();
 
export default Users;