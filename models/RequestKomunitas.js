import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import Rumahtangga from "../models/RumahtanggaModel.js";

const { DataTypes } = Sequelize;
 
const request_to_kom = db.define('request_to_komunitas',{
    idrumahtangga:{
        type: DataTypes.INTEGER
    },
    idkomunitas:{
        type: DataTypes.INTEGER
    },
    status:{
        type: DataTypes.INTEGER
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();
 
export default request_to_kom;