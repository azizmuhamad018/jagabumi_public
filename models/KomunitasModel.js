import { Sequelize } from "sequelize";
import db from "../config/Database.js";
 
const { DataTypes } = Sequelize;
 
const Komunitas = db.define('users_komunitas',{
    nama_komunitas:{
        type: DataTypes.STRING
    },
    nama_ketua:{
        type: DataTypes.STRING
    },
    jenis_kelamin:{
        type: DataTypes.STRING
    },
    tempat_lahir:{
        type: DataTypes.STRING
    },
    tanggal_lahir:{
        type: DataTypes.DATE
    },
    tanggal_bergabung:{
        type: DataTypes.DATE
    },
    tanggal_rutin_pickup:{
        type: DataTypes.DATE
    },
    alamat:{
        type: DataTypes.STRING
    },
    no_wa:{
        type: DataTypes.STRING
    },
    no_rek:{
        type: DataTypes.STRING
    },
    nama_bank:{
        type: DataTypes.STRING
    },
    ktp:{
        type: DataTypes.STRING
    },
    komitmen:{
        type: DataTypes.STRING
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();
 
export default Komunitas;