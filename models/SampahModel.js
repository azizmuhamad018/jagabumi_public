import { Sequelize } from "sequelize";
import db from "../config/Database.js";
 
const { DataTypes } = Sequelize;
 
const Sampahkeluarga = db.define('sampah',{
    idanggotakeluarga:{
        type: DataTypes.INTEGER
    },
    idkomunitas:{
        type: DataTypes.INTEGER
    },
    tanggal_pengumpulan:{
        type: DataTypes.DATE
    },
    jenis_sampah:{
        type: DataTypes.STRING
    },
    total_nilai:{
        type: DataTypes.STRING
    },
    status:{
        type: DataTypes.INTEGER
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();
 
export default Sampahkeluarga;