import { Sequelize } from "sequelize";
import db from "../config/Database.js";
import Rumahtangga from "../models/RumahtanggaModel.js";

const { DataTypes } = Sequelize;
 
const rekap_sampah = db.define('users_rekap_sampah',{
    idrumahtangga:{
        type: DataTypes.STRING
    },
    idkomunitas:{
        type: DataTypes.STRING
    },
    foto:{
        type: DataTypes.STRING
    },
    jenis_sampah:{
        type: DataTypes.STRING
    },
    harga_per_kilo:{
        type: DataTypes.STRING
    },
    jumlah:{
        type: DataTypes.STRING
    },
    sub_total:{
        type: DataTypes.STRING
    },
    estimasi_total:{
        type: DataTypes.STRING
    },
    permintaan_pengambilan:{
        type: DataTypes.DATE
    },
    jenis_pembayaran:{
        type: DataTypes.STRING
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();

export default rekap_sampah;