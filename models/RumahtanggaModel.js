import { Sequelize } from "sequelize";
import db from "../config/Database.js";
 
const { DataTypes } = Sequelize;
 
const Rumahtangga = db.define('users_rumahtangga',{
    idkom:{
        type: DataTypes.INTEGER
    },
    nama:{
        type: DataTypes.STRING
    },
    jenis_kelamin:{
        type: DataTypes.STRING
    },
    tempat_lahir:{
        type: DataTypes.STRING
    },
    tanggal_lahir:{
        type: DataTypes.DATE
    },
    tanggal_bergabung:{
        type: DataTypes.DATE
    },
    alamat:{
        type: DataTypes.STRING
    },
    no_wa:{
        type: DataTypes.STRING
    },
    no_rek:{
        type: DataTypes.STRING
    },
    nama_bank:{
        type: DataTypes.STRING
    },
    ktp:{
        type: DataTypes.STRING
    },
    komitmen:{
        type: DataTypes.STRING
    }
},{
    freezeTableName:true
});
 
(async () => {
    await db.sync();
})();
 
export default Rumahtangga;