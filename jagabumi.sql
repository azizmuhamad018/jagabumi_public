-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 29 Nov 2022 pada 08.32
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jagabumi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `request_to_komunitas`
--

CREATE TABLE `request_to_komunitas` (
  `id` int(11) NOT NULL,
  `idrumahtangga` int(11) DEFAULT NULL,
  `idkomunitas` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `levelid` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_wa` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status_approve` int(11) DEFAULT NULL,
  `refresh_token` text DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `userid`, `levelid`, `nama`, `no_wa`, `password`, `status_approve`, `refresh_token`, `createdAt`, `updatedAt`) VALUES
(1, 1, 4, 'Muh Aziz', '082133619739', '$2b$10$LVsduOORtdkC.S4xoVXMk.tDQDGnQ70YvOi2KSS/9cc9PpwK3/jCy', 1, NULL, '2022-11-15 03:37:37', '2022-11-15 03:37:37'),
(2, 1, 5, 'Rumah Tangga', '082133619736', '$2b$10$DkdEFuona1vfpaTPw8sySOnaFkrzDE7k70NHnNsn8wS8vbc5.61Ym', 1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZCI6MiwidXNlcklkIjoxLCJsZXZlbElkIjo1LCJuYW1hIjoiUnVtYWggVGFuZ2dhIiwibm9fd2EiOiIwODIxMzM2MTk3MzYiLCJpYXQiOjE2Njg0ODM1NzUsImV4cCI6MTY2ODU2OTk3NX0.PKfbGYCetUk3Bdq4Fi2HZpXWGMq4ymOZA-kKF-tipeY', '2022-11-15 03:37:50', '2022-11-15 03:39:35');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_komunitas`
--

CREATE TABLE `users_komunitas` (
  `id` int(11) NOT NULL,
  `nama_komunitas` varchar(255) DEFAULT NULL,
  `nama_ketua` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  `tanggal_bergabung` datetime DEFAULT NULL,
  `tanggal_rutin_pickup` datetime DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_wa` varchar(255) DEFAULT NULL,
  `no_rek` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `ktp` varchar(255) DEFAULT NULL,
  `komitmen` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_komunitas`
--

INSERT INTO `users_komunitas` (`id`, `nama_komunitas`, `nama_ketua`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `tanggal_bergabung`, `tanggal_rutin_pickup`, `alamat`, `no_wa`, `no_rek`, `nama_bank`, `ktp`, `komitmen`, `createdAt`, `updatedAt`) VALUES
(1, 'RW 4', 'Muh Aziz', 'Laki - Laki', 'Purbalingga', '1996-11-11 00:00:00', '2022-11-11 00:00:00', '2022-11-11 00:00:00', 'Purbalingga', '082133619739', '97432551054', 'BCA', '3303091111960002', '1', '2022-11-15 03:37:37', '2022-11-15 03:37:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_rekap_sampah`
--

CREATE TABLE `users_rekap_sampah` (
  `id` int(11) NOT NULL,
  `idrumahtangga` varchar(255) DEFAULT NULL,
  `idkomunitas` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `jenis_sampah` varchar(255) DEFAULT NULL,
  `harga_per_kilo` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `sub_total` varchar(255) DEFAULT NULL,
  `estimasi_total` varchar(255) DEFAULT NULL,
  `permintaan_pengambilan` datetime DEFAULT NULL,
  `jenis_pembayaran` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_rekap_sampah`
--

INSERT INTO `users_rekap_sampah` (`id`, `idrumahtangga`, `idkomunitas`, `foto`, `jenis_sampah`, `harga_per_kilo`, `jumlah`, `sub_total`, `estimasi_total`, `permintaan_pengambilan`, `jenis_pembayaran`, `createdAt`, `updatedAt`) VALUES
(1, '1', '1', 'HqkjkODvwfYElqRybG2mo.jpeg', 'PET', '3000', '2', '6000', '6000', '2022-11-14 00:00:00', 'Poin', '2022-11-15 03:42:58', '2022-11-15 03:42:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_rumahtangga`
--

CREATE TABLE `users_rumahtangga` (
  `id` int(11) NOT NULL,
  `idkom` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  `tanggal_bergabung` datetime DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_wa` varchar(255) DEFAULT NULL,
  `no_rek` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `ktp` varchar(255) DEFAULT NULL,
  `komitmen` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users_rumahtangga`
--

INSERT INTO `users_rumahtangga` (`id`, `idkom`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `tanggal_bergabung`, `alamat`, `no_wa`, `no_rek`, `nama_bank`, `ktp`, `komitmen`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Rumah Tangga', 'Laki - Laki', 'Purbalingga', '1996-11-11 00:00:00', '2022-11-09 00:00:00', 'Desa Pakuncen RT 04/01', '082133619736', '0970847855', 'BCA', '3303091111963050', '1', '2022-11-15 03:37:50', '2022-11-15 03:42:03');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `request_to_komunitas`
--
ALTER TABLE `request_to_komunitas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users_komunitas`
--
ALTER TABLE `users_komunitas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users_rekap_sampah`
--
ALTER TABLE `users_rekap_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users_rumahtangga`
--
ALTER TABLE `users_rumahtangga`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `request_to_komunitas`
--
ALTER TABLE `request_to_komunitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users_komunitas`
--
ALTER TABLE `users_komunitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users_rekap_sampah`
--
ALTER TABLE `users_rekap_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users_rumahtangga`
--
ALTER TABLE `users_rumahtangga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
