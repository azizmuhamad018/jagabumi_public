import express from "express";
import { getUsers, RegisterKomunitas, RegisterRT, Login, Logout, ChangePass } from "../controllers/Users.js";
import { inputSampah, ListForCommunity, ListByRT } from "../controllers/Plastik.js";
import { bioRT } from "../controllers/RumahTangga.js";
import { getKomunitas, getRequestKomunitas, updateRequestKomunitas, bioKomunitas } from "../controllers/Komunitas.js";
import { verifyToken } from "../middleware/VerifyToken.js";
import { refreshToken } from "../controllers/RefreshToken.js";
import multer from 'multer';
import path from 'path';
import {fileURLToPath} from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
import { nanoid } from 'nanoid';
import * as mime from 'mime-types';

const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, "../images"));
    },
    // konfigurasi penamaan file yang unik
    filename: function (req, file, cb) {
        let id = nanoid();
        let ext = mime.extension(file.mimetype);
      cb(
        null,
        `${id}.${ext}`
      );
    },
  });

const router = express.Router();

// login & Register
router.post('/register-komunitas', RegisterKomunitas);
router.post('/register-rt', RegisterRT);
router.post('/login', Login);
router.post('/change-password', ChangePass);
router.get('/token', refreshToken);
router.delete('/logout', Logout);
router.get('/users', verifyToken, getUsers);

// Komunitas dan Rumah Tangga
router.get('/daftar-komunitas', verifyToken, getKomunitas);
router.get('/daftar-request-komunitas', verifyToken, getRequestKomunitas);
router.get('/update-request-komunitas', verifyToken, updateRequestKomunitas);
router.post('/update-bio-komunitas', verifyToken, bioKomunitas);
router.post('/update-bio-rt', verifyToken, bioRT);

// Input Transaksi
router.post('/input-sampah', verifyToken, multer({ storage: diskStorage }).single('foto_sampah'), inputSampah);
router.get('/list-for-community', verifyToken, ListForCommunity);
router.get('/list-for-rt', verifyToken, ListByRT);

// E-wallet
export default router;